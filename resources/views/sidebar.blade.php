<div id="sidebar">
	<nav class="navbar navbar-default" role="navigation">
		<div class="navbar-header">
		<a id="brand" class="navbar-brand" href="#home" du-smooth-scroll>
			<img src="{{ asset('img/logo.png') }}" alt="logo">
			<h2>BABY BLUES 24</h2>
		</a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Włącz/wyłącz nawigację</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>

		<div id="sidebar-items" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li du-scrollspy="about-me">
					<a class="sscroll" href="#about-me" du-smooth-scroll>O mnie</a>
				</li>
				<li du-scrollspy="offer">
					<a class="sscroll" href="#offer" du-smooth-scroll>Oferta</a>
				</li>
				<li du-scrollspy="price-table">
					<a class="sscroll" href="#price-table" du-smooth-scroll>Cennik</a>
				</li>
				<li du-scrollspy="contact">
					<a class="sscroll" href="#contact" du-smooth-scroll>Kontakt</a>
				</li>
				<li>
					<a class="sscroll" href="http://babyblues24.blogspot.com/" target="_blank">Blog</a>
				</li>
			</ul>
			<a class="nav-append" target="_blank" href="https://www.facebook.com/pages/Babyblues24/572940799470389">
				<img id="facebook-icon" src="{{ asset('img/fb-icon-240px.png') }}" alt="facebook-logo">
			</a>
		</div>
	</nav>
</div>
