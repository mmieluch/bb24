@extends('master')

@section('content')
	@include('content.home')
	@include('content.about_me')
	@include('content.offer')
	@include('content.price_table')
	@include('content.contact')
@stop
