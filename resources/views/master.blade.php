<!doctype html>
<html lang="en">
	<head>
		<base href="/"/>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="{{ asset('favicon.icon') }}" type="image/x-icon">
		<link rel="icon" href="{{ asset('favicon.icon') }}" type="image/x-icon">

		<title>BabyBlues24</title>
		<link rel="stylesheet" href="{{ asset('css/bb24.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/garis-light.min.css')  }}">
	</head>
	<body>
		<div id="main-wrapper">
			<a name="top" id="top"></a>
			@include('sidebar')
			<div id="container">
				@yield('content')
			</div>
		</div>
		<div class="totop" id="backtotop">
			<span>
				<a href="#home" class="first sscroll" du-smooth-scroll>
					<i class="fa fa-angle-up"></i>
				</a>
				<a href="#home" class="hover sscroll" du-smooth-scroll>
					<i class="fa fa-angle-up"></i>
				</a>
			</span>
		</div>

		<script src="js/vendor/require.js" data-main="js/boot.js"></script>
		<!--[if lt IE 9]>
			<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<![endif]-->
	</body>
</html>
