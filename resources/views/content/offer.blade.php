<section id="offer" class="odd">
	<div class="row">
		<div class="col-md-12">
			<div class="header-content">
				<h2>Oferta</h2>
			</div>
			<div class="row">
				<div class="offer-main-text col-sm-12 col-md-6">
					<p>Jeśli jesteś na tej stronie, to znaczy, że szukasz pomocy lub informacji na temat psychologicznych aspektów planowania rodziny, przeżywania ciąży i macierzyństwa. Baby Blues 24 powstało z myślą o Tobie.</p>
					<p>Jako psycholog pomagam mamie, tacie, rodzinom w tym pięknym, lecz zarazem trudnym okresie planowania, oczekiwania i powitania na świecie nowego członka rodziny.</p>
					<p>Ten aspekt ludzkiego życia ma wiele wymiarów i może nieść ze sobą zarówno chwile pełne szczęścia i radości, jak i zwątpienia oraz lęku.</p>
					<p>Niezależnie od sytuacji w której się znajdujesz i emocji, które Tobą targają, zawsze jestem gotowa, aby wspomóc Cię, wysłuchać i pomóc znaleźć rozwiązanie dla Twoich problemów.</p>
					<p>Moja oferta jest skierowana zarówno do kobiet w ciąży i połogu, młodych mam i tatusiów oraz par starających się o dziecko.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<p>Wiem, że ciąża może być trudnym przeżyciem. Czasem towarzyszy nam lęk o nienarodzone dziecko, obawy przed porodem czy wątpliwości związane ze zmianami, jakie w naszym życiu wprowadzi nowy członek rodziny. Te odczucia są naturalne, ale czasami trudno poradzić sobie z nimi samodzielnie. Baby blues 24 powstało po to, by pomóc Ci uporać się z tymi emocjami.</p>
					<p>Takie obawy mogą dotyczyć również wszystkich innych członków rodzinny. Aby mogli stać się wsparciem dla mamy, oferuję pomoc także dla nich w radzeniu sobie z tą nową sytuacją.</p>
				</div>
				<div class="col-md-12">
					<div class="header-content">
						<h3>Spotkania indywidualne</h3>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-8">
					<h4>Kobieta w ciąży</h4>
					<p>W trakcie oczekiwania na narodziny dziecka, mogą towarzyszyć Ci różne emocje, zarówno pozytywne jak i negatywne. Może pojawić się moment, w którym zaczniesz odczuwać lęk przed porodem. Wspólnie znajdziemy sposób, aby odsunąć go na dalszy plan, co pozwoli Ci spokojnie oczekiwać narodzin Twojego dziecka.</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 exposed">
					<p>Nasze spotkania mają na celu zmierzenie się z trudnościami, które niesie za sobą okres ciąży, połogu i&nbsp;zmian zachodzących w życiu kobiety w&nbsp;okresie wczesnego macierzyństwa.</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-8">
					<h4>Młoda mama</h4>
					<p>Po urodzeniu dziecka mogą pojawić się u Ciebie przykre emocje, których się nie spodziewałaś i nie chcesz odczuwać, takie jak smutek, poczucie winy, poczucie braku kompetencji w roli mamy.  Ogromne  zmęczenie i wrażenie, że nie radzisz sobie z opieką nad dzieckiem. Możesz mieć trudności z podjęciem najprostszej decyzji. To naturalne. Syndrom baby blues może przebiegać  z różnym nasileniem. Czasami wsparcie najbliższych jest wystarczające dla młodej mamy, niestety coraz częściej kobiety czują się nierozumiane, rozdrażnione, samotne w swoim problemie . Czasami przebieg baby blues jest tak trudny, że może przerodzić się w depresję poporodową. Wtedy spotkanie i rozmowa z psychologiem może pomóc młodej mamie zrozumieć i uporać się z towarzyszącymi jej emocjami. Nasze spotkania mają na celu polepszenie Twojego komfortu w tym trudnym okresie i  znalezienie odpowiedzi na nurtujące Cię pytania.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="header-content">
						<h3>Wsparcie dla rodziny</h3>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<h4>Zostaniesz ojcem</h4>
					<p>To wyjątkowe uczucie. Czasami jednak mimo wielkiej radości związanej z taką nowiną, pojawiają się również obawy związane z odpowiedzialnością która spadnie na Twoje barki. Wsparcie dla mamy w czasie ciąży, porodu i wczesnego macierzyństwa to trudne wyzwanie. Jeżeli oczekujesz odpowiedzi na nurtujące Cię pytania związane z zadaniami, z którymi przyjdzie Ci się zmierzyć, z pewnością wspólnie znajdziemy na nie odpowiedź.</p>
				</div>
				<div class="col-xs-12 col-sm-6">
					<h4>Bliska Ci osoba spodziewa się dziecka</h4>
					<p>Chcesz być dla niej wsparciem i dzielić z nią ten radosny czas. Nie zawsze jednak wiesz, czego potrzebuje lub jak jej pomóc? Również dla najbliższych służę poradą,  jak wesprzeć młodą mamę.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="header-content">
						<h3>Wykłady</h3>
					</div>
				</div>
				<div class="col-xs-12 col-md-8">
					<p>Moja działalność ma również na celu poszerzanie wiedzy dotyczącej psychologicznych aspektów planowania rodziny, przeżywania ciąży i macierzyństwa. Na spotkaniach grupowych chcę podzielić się z Wami moją wiedzą na temat emocji, które mogą towarzyszyć przyszłej mamie, wiedzą na temat baby blues i depresji poporodowej.</p>
				</div>
				<div class="col-xs-12 col-md-4 exposed">
					<p>Zachęcam do kontaktu szkoły rodzenia i kluby mam, które chciałyby poszerzyć swoją ofertę o&nbsp;warsztaty psychologiczne.</p>
				</div>
			</div>
		</div>
	</div>
</section>
