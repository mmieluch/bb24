<section id="price-table">
	<div class="row">
		<div class="col-md-12">
			<div class="header-content">
				<h2>Cennik</h2>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<h3>Spotkania indywidualne</h3>
					<p>Z myślą o wyjątkowej sytuacji i jak największym komforcie moich klientów proponuję spotkania indywidualne w ich domach. W znanym i bezpiecznym otoczeniu. Oczywiście możemy spotkać się w innym, wybranym przez Ciebie miejscu.</p>
					<p class="text-center">
						<strong>150 zł/2h</strong><br><small>z dojazdem w obrębie Krakowa</small>
					</p>
				</div>
				<div class="col-xs-12 col-md-6">
					<h3>Prelekcje, wykłady, warsztaty</h3>
					<p>Zainteresowane kluby mam i szkoły rodzenia proszę o kontakt w celu omówienia warunków.</p>
				</div>
			</div>
		</div>
	</div>
</section>
