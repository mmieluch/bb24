<section id="about-me" class="clearfix">
	<div class="row">
		<div class="col-md-6 mg-bt-80">
			<div class="header-content">
				<h2>O mnie</h2>
			</div>
			<p>Witaj!</p>
			<p>Od wielu lat, w ramach prywatnej praktyki oraz jako psycholog Caritas Archidiecezji Krakowskiej, towarzyszę i&nbsp;pomagam wielu osobom w poszukiwaniu dobrych rozwiązań ich problemów.</p>
			<p>Od początku mojej pracy terapeutycznej specjalizuję się w&nbsp;pracy z&nbsp;parami i&nbsp;rodzinami, zaś w&nbsp;tej wyjątkowej przestrzeni &mdash; we&nbsp;wszystkim co wiąże się z&nbsp;pomocą mamie i&nbsp;jej rodzinie w&nbsp;sytuacjach związanych z planowaniem, oczekiwaniem i&nbsp;z&nbsp;pojawieniem się nowego życia. Ponieważ sama jestem mamą, wiem, jak ogromne znaczenie dla kobiety ma wsparcie w okresie wczesnego macierzyństwa. Obok mamy również tata, rodzina lub przyjaciele  mogą potrzebować pomocy i&nbsp;porady, gdyż dla nich wszystkich pojawienie się malutkiego człowieka może być okresem bardzo pięknym, ale również szalenie trudnym.</p>
			<p>Jeśli czujesz, że&nbsp;któryś z&nbsp;tych problemów Cię dotyczy, że&nbsp;potrzebujesz wsparcia, że&nbsp;chcesz podzielić się swoimi wątpliwościami, że&nbsp;chcesz z&nbsp;kimś porozmawiać i&nbsp;uzyskać pomoc &mdash; zapraszam do spotkania i&nbsp;rozmowy.</p>
			<p class="text-right">Anna Czajkowska</p>
		</div>
		<div class="col-md-6 mg-bt-80" id="about-me-photo">
			<img src="{{ asset('img/aczajkowska_photo.jpg') }}" alt="anna-czajkowska-photo">
			<p class="photo-description">Jestem psychologiem z wieloletnim doświadczeniem w pracy terapeutycznej, absolwentką psychologii stosowanej na Uniwersytecie Jagiellońskim.</p>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
