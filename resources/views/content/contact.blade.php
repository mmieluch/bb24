<section id="contact" class="odd">
	<div id="contact-content">
		<div class="row">
			<div class="col-md-12">
				<div class="header-content">
					<h2>Kontakt</h2>
				</div>
				<address>
					<strong>Baby Blues 24</strong><br>
					Anna Czajkowska<br>
					ul. Misiołka 1<br>
					31&ndash;525 kraków<br>
					<br>
					<abbr title="Telefon"><i class="fa fa-phone"></i></abbr>
						602 609 101<br>
					<abbr title="E&ndash;mail"><i class="fa fa-envelope"></i></abbr>
						<a href="mailto:bb24pl@gmail.com">bb24pl@gmail.com</a>
				</address>
			</div>
		</div>
	</div>
	<div id="map"
    		 class="map"
    		 data-lat="50.067287"
    		 data-lng="19.9599676"
    		 data-address="<strong>Baby Blues 24</strong><br><br>ul. Misiołka 1<br>31&ndash;525 Kraków"
    		 data-color="#26292e"
    		 bb24-map
    		 >
    </div>
</section>
