<?php

return [

	// List of public assets that need to SCP'd to a remote server
	'public_assets' => array(
		'public/css/garis-light.min.css',
		'public/img/aczajkowska_photo.jpg',
		'public/img/ChromaStock_13808783.jpg',
	),

];
