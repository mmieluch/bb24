(function(define) {
	'use strict';

	var deps = [
		'angular',

		'./MapDirective'
	];

	define(deps, function(angular, MapDirective) {

		var moduleName = 'ContactModule';

		angular.module(moduleName, [])
			.directive('bb24Map', MapDirective)
		;

		return moduleName;
	});

}(define));
