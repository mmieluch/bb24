(function(define) {
	'use strict';

	var deps = [
		'angular'
	];

	define(deps, function() {

		var ContactController = function($scope) {
			$scope.lats = angular.element("#map").attr('data-lat');
			$scope.lngs = angular.element("#map").attr('data-lng');
			$scope.data_address = angular.element("#map").attr('data-address');
			$scope.color = angular.element("#map").attr('data-color');
			$scope.saturation = 100;
		};

		return ['$scope', ContactController];

	});

}(define));
