(function(define) {
	'use strict';

	var deps = [
		'async!//maps.google.com/maps/api/js?sensor=false'
	];

	define(deps, function() {

		var directive = function() {

			var getStyles = function(color, saturation) {
				return [{
					featureType: "landscape",
					stylers: [{
						hue: "#000"
					}, {
						saturation: -100
					}, {
						lightness: 40
					}, {
						gamma: 1
					}]
				}, {
					featureType: "road.highway",
					stylers: [{
						hue: color
					}, {
						saturation: saturation
					}, {
						lightness: 20
					}, {
						gamma: 1
					}, {
						visibility: "simplified"
					}]
				}, {
					featureType: "road.arterial",
					stylers: [{
						hue: color
					}, {
						saturation: saturation
					}, {
						lightness: 20
					}, {
						gamma: 1
					}, {
						visibility: "simplified"
					}]
				}, {
					featureType: "road.local",
					stylers: [{
						hue: color
					}, {
						saturation: saturation
					}, {
						lightness: 50
					}, {
						gamma: 1
					}, {
						visibility: "simplified"
					}]
				}, {
					featureType: "water",
					stylers: [{
						hue: "#000"
					}, {
						saturation: -100
					}, {
						lightness: 15
					}, {
						gamma: 1
					}]
				}, {
					featureType: "poi",
					stylers: [{
						hue: "#000"
					}, {
						saturation: -100
					}, {
						lightness: 25
					}, {
						gamma: 1
					}, {
						visibility: "simplified"
					}]
				}, {
					featureType: "road",
					elementType: "labels",
					stylers: [{
						visibility: "off"
					}]
				}];
			};

			var setupMap = function(element, opts, styles) {
				var styledMap = new google.maps.StyledMapType(styles, {
					name: "Baby Blues 24"
				});
				var map = new google.maps.Map(element, opts);

				map.mapTypes.set('bb24', styledMap);
				map.setMapTypeId('bb24');

				return map;
			};

			var setupTooltip = function(address) {
				return new google.maps.InfoWindow({
					content: address
				});
			};

			var setupCoordinates  = function(lat, lng) {
				return new google.maps.LatLng(lat, lng);
			};

			var setupMarker = function(coordinates, img, infowindow) {
				return new google.maps.Marker({
					position: coordinates,
					icon: img
				});
			};

			var setupInfowindowListener = function(map, infowindow, marker) {
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map, marker);
				});
			};

			var setupOptions = function(opts) {
				var defaults = {
					zoom: 15,
					navigationControl: 0,
					scrollwheel: false,
					zoomControl: 0,
					disableDefaultUI: true,
					streetViewControl: 0,
					draggable: 0
				};

				return angular.extend({}, defaults, opts);
			};

			var link = function($scope, $elem, $attrs) {
				var address = $attrs.address;
				var color = $attrs.color;
				var saturation = 100;
				var lat = $attrs.lat;
				var lng = $attrs.lng;
				var markerImage = 'img/map-marker.png';

				var coordinates = setupCoordinates(lat, lng);
				var styles = getStyles(color, saturation);
				var mapOptions = setupOptions({
					center: coordinates,
					zoom: 15,
					mapTypeId: 'bb24'
				});

				$scope.map = setupMap($elem.get(0), mapOptions, styles);
				$scope.marker = setupMarker(coordinates, markerImage);
				$scope.infowindow = setupTooltip(address);

				$scope.marker.setMap($scope.map);
				setupInfowindowListener($scope.map, $scope.infowindow, $scope.marker);
			};

			return {
				'link': link
			}

		};

		return [directive];

	});

}(define));
