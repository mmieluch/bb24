(function(define) {
	'use strict';

	var deps = [
		'jquery',
		'angular',
		'angular-scroll',

		'app/modules/contact/ContactModule'
	];

	define(deps, function($, angular, duScroll, ContactModule) {

		var app, appName = 'bb24';

		app = angular.module(appName, [
				'duScroll',
				'ContactModule'
			])
			.config(function($locationProvider) {
				$locationProvider.html5Mode(true);
			})
			.run(function($rootScope, $location) {
				// Update hashes every tme duScrollSpy communicates, that a new element has become
				// active.
				$rootScope.$on('duScrollspy:becameActive', function($event, $element) {
					var link = $element.find('a');
					var hash = link.prop('hash');

					if (hash) {
						// Remove the hash (#) sign.
						hash = hash.substr(1).replace();
						$location.hash(hash);
						$rootScope.$apply();
					}
				});
			})
			.value('duScrollEasing', function (t) {
				// easeInOutQuart
				return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t
			})
			.value('duScrollDuration', 500)
		;

		var body = $('body').get(0);
		angular.bootstrap($('body').get(0), [appName]);

		return app;
	});

}(define));
