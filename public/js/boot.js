(function(require) {

	require.config({
		baseUrl: 'js',
		paths: {
			jquery: [
				'//code.jquery.com/jquery-2.1.1',
				'vendor/jquery'
			],
			angular: [
				'//ajax.googleapis.com/ajax/libs/angularjs/1.3.1/angular.min',
				'vendor/angular.min'
			],
			'angular-scroll': ['vendor/angular-scroll.min'],
			'async': 'vendor/async'
		},
		shim: {
			jquery: {
				exports: 'jquery'
			},
			angular: {
				exports: 'angular',
				deps: ['jquery']
			},
			'angular-scroll': {
				exports: 'duScroll',
				deps: ['angular']
			}
		},
		urlArgs: 'v=1.1'
	});

	require(['app/main'], function(app) {});

})(require);
