//------------------------------
// Variables - Gulp modules
//------------------------------
var gulp        = require('gulp'),
	less        = require('gulp-less'),
	watch       = require('gulp-watch'),
	add         = require('gulp-add-src'),
	concat      = require('gulp-concat'),
	miniCss     = require('gulp-minify-css'),
	autoprefix  = require('gulp-autoprefixer'),
	gutil       = require('gulp-util'),
	rjs         = require('gulp-rjs')
;

//------------------------------
// Private functions
//------------------------------
/**
 * Definition function for processing LESS and CSS files.
 *
 * This function performs:
 * - processing LESS files;
 * - autoprefixing CSS directives;
 * - concatenating the output with given CSS files;
 * - minifying CSS;
 * - saving the processed file in the public/css directory.
 *
 * @param gulp Instance of Gulp object
 * @param cssToAdd Array of paths to files to concatenate
 * @param outputFileName File name
 * @returns Gulp
 * @private
 */
var _cssProcessingFunction = function(g, cssToAdd, outputFileName) {
	var processed = g.pipe(less().on('error', function(event) {
			gutil.log(event);
			return true;
		}))
		.pipe(autoprefix('last 5 version', 'ie 8', 'ie 9'))
		.pipe(add(cssToAdd))
		.pipe(concat(outputFileName))
		.pipe(miniCss())
		.pipe(gulp.dest('public/css'))
		;
};

//------------------------------
// Processing custom files
//------------------------------
/** @var array Paths (in glob format) to custom Less files */
var customLessFiles = [
	'resources/assets/custom/less/*.less'
];
/** @var array Paths (in glob format) to custom CSS files to concatenate */
var customCssFiles = [
	'resources/assets/templates/garis-light/css/bootstrap.css',
	'resources/assets/templates/garis-light/css/bootstrap-theme.css',
	'resources/assets/templates/garis-light/css/font-awesome.css'
];

// Task processing custom project files.
gulp.task('bb24', function() {
	var g = gulp.src(customLessFiles);
	return _cssProcessingFunction(g, customCssFiles, 'bb24.min.css');
});

//------------------------------
// Processing template files
//------------------------------
/** @var array Paths (in glob format) to template Less files */
var tplLessFiles = [
	'resources/assets/templates/garis-light/less/blue.less'
];
/** @var array Paths (in glob format) to template CSS files to concatenate */
var tplCssFiles = [
	'resources/assets/templates/garis-light/css/style.css'
];

// Task processing template files.
gulp.task('tpl', function() {
	var g = gulp.src(tplLessFiles);
	return _cssProcessingFunction(g, tplCssFiles, 'garis-light.min.css');
});

//------------------------------
// Javascript
//------------------------------
/** @var array Paths (in glob format) to JavaScript files to concatenate */
var jsFiles = [];

//------------------------------
// Watchers
//------------------------------
gulp.task('watch', ['bb24'], function() {
	gulp.watch(customLessFiles, ['bb24']);
});

//------------------------------
// Definition of a default task.
//------------------------------
gulp.task('default', ['bb24', 'tpl']);
